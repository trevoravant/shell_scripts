#!/bin/bash
thisWID=$(xdotool getactivewindow)
termWID="$(xdotool search --onlyvisible gnome-terminal | sort -n | tail -n1)"
xdotool windowactivate $termWID
xdotool key Control+d
xdotool key Control+d
xdotool windowactivate $thisWID
