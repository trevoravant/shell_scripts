#!/bin/zsh
# this script will iterate through all .VOB files in the video directory of a DVD and
# convert them to 960x720 (720p) resolution with pixel aspect ratio equal to
# one, so that they can be uploaded to Youtube
# 
# example
# dvd_convert.sh /path/to/vob/files/
#
cd $1
for file_vob in *.VOB; do
	file_mp4=${file_vob:r}.mp4
	ffmpeg -i $file_vob -vf "scale=960x720:flags=lanczos,setsar=1" -c:v libx264 $file_mp4
done
cd -
