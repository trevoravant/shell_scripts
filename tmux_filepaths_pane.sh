#!/bin/zsh 
# read the content of a tmux target pane named "output",
# determine if there are any filenames with line numbers,
# and if so, open that file in vim 
# 
# example filename+linenumber output for different commands:
#
#  File "/home/trevor/file.txt", line 13, in <module>  | python
#file.txt:20:Course I've been humbled by the void      | grep
#
# usage:
#   the sole input to the script is the index of the found filenames to open,
#   from bottom-to-top 
#
# $ tmux_filenames_pane.sh 2
#
output=$(tmux capture-pane -pJ -t output)
echo $output > /home/trevor/Downloads/save_test.txt
wd="$(tmux display-message -p -t output '#{pane_current_path}')/"
save_file=/home/trevor/Downloads/saver.txt
filenames=()
linenums=()
while IFS= read line 
do 
	# python outputs
    if echo "$line" | grep -q '^  File "[a-zA-Z0-9_\/\.\-]*\.py", line [0-9]*, in .*'; then
		filename=$(echo $line | sed 's/^  File "\([a-zA-Z0-9_\/\.\-]*\.py\)", line [0-9]*, in .*/\1/')
		linenum=$(echo $line | sed 's/^  File "[a-zA-Z0-9_\/\.\-]*\.py", line \([0-9]*\), in .*/\1/')
		filenames+=($filename)
		linenums+=($linenum)
		echo $filename >> $save_file
		echo $linenum >> $save_file
	# grep outputs
	elif echo $line | grep -q '^[a-zA-Z0-9_\/\.\-]*:[0-9]*:.*'; then
		filename=$(echo $line | sed 's/^\([a-zA-Z0-9_\/\.\-]*\):[0-9]*:.*/\1/')
		linenum=$(echo $line | sed 's/^[a-zA-Z0-9_\/\.\-]*:\([0-9]*\):.*/\1/')
		filenames+=($wd$filename)
		linenums+=($linenum)
		echo $filename >> $save_file
		echo $linenum >> $save_file
	fi
done <<< "$output"
if [[ -z $filenames ]]; then
	>&2 echo NO FILENAMES FOUND
elif (($1 > ${#filenames})); then 
	>&2 echo INPUT GREATER THAN NUMBER OF FILENAMES
else
	#echo $filenames[-$1]
	#echo $linenums[-$1]
	filename=$filenames[-$1]
	linenum=$linenums[-$1]
	vim_cmd='if vim --serverlist | grep -q VIM; then vim --remote +'$linenum' '$filename'; else kgx -- vim +'$linenum' '$filename'; fi'
	echo $vim_cmd
	eval ${vim_cmd}
fi
