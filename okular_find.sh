#!/bin/bash
# inputs: path of pdf, line number, path of tex file
#
# for this to work, the pdf must be compiled with synctex:
#     pdflatex --synctex=1 /path/to/file.tex
#
# the okular command to open a pdf to a certain line in the tex file is:
#     okular --unique "/path/to/file.pdf#src:31 /path/to/file.tex"
# where 31 is an example of a line number
this_WID=$(xdotool getactivewindow)
okular --unique "$1#src:$2 $3"
xdotool windowactivate $this_WID
