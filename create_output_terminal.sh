#!/bin/zsh
# create a terminal titled "output" and start a tmux session named "output" in it,
# if there is no tmux session named "output",
# or if there is an unattached tmux session named "output"
# https://unix.stackexchange.com/a/177598/294686
tmux new-session -d -s output # this may fail, but that's ok
num_attached=$(tmux ls -F "#{session_name} #{session_attached}" | grep output | awk '{print $2}')
if [ $num_attached -eq 0 ]; then
	gnome-terminal -- /bin/sh -c 'printf "\e]2;output\a"; tmux attach -t output; exec zsh'
fi
