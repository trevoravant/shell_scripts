# input a string and determine if there are any filepaths with line numbers,
# if so, open that file in vim
# 
# example filepath-linenumber output for different commands:
#/home/trevor/file.txt blah blah blah                       | absolutepath (find <directoryname>)
#./trevor/file.txt blah blah blah                           | relativepath (find)
#/home/trevor/file.txt:20:3 error: blah blah blah           | absolutepath:linenum: (cmake)
#file.txt:20:blah blah blah                                 | relativepath:linenum: (grep, g++)
#  File "/home/trevor/file.txt", line 13 blah blah blah     | python
# git diff: @@ -4,6 +5,7 @@                                 | git diff
#l.13 blah blah blah                                        | pdflatex & latexmk
#    at /home/trevor/file.txt:20                            | at absolutepath:linenum (gdb)
#
# inputs:
#   the sole input to the script is a string representing the line to be processed
#
# example usage
# $ open_filepath.sh "/home/trevor/file.txt:20: apple banana"
# $ echo "/home/trevor/file.txt:20: apple banana" | xargs -d '\n' open_filepath.sh
#
# notes:
# https://gitlab.com/trevoravant/notes/-/blob/master/clickable_files_tmux.md

# extract abspath and linenum if found, then open abspath to linenum
open_file () {
	abspath=$(cut -d ' ' -f 1 <<< $abspath_and_linenum)
	linenum=$(cut -d ' ' -f 2 <<< $abspath_and_linenum)
	vim_cmd='if vim --serverlist | grep -q VIM; then vim --remote +'$linenum' '$abspath'; else gnome-terminal -- vim +'$linenum' '$abspath'; fi'
	eval ${vim_cmd}
	exit 0
}

# first, remove all characters except ascii characters
# https://unix.stackexchange.com/a/403021/294686
line=$(tr -cd '\000-\177' <<< $1)

# absolutepath
# note: the sed command uses extended regex
abspath=$(sed -nE 's/^(\/[a-zA-Z0-9_\/\.\-]+)($| .*$)/\1/p' <<< $line)
if [[ -n $abspath ]]; then
	abspath_and_linenum=$abspath' 0'
	open_file
fi

# ./relativepath
relpath=$(sed -nE 's/^\.\/([a-zA-Z0-9_\/\.\-]+)($| .*$)/\1/p' <<< $line)
if [[ -n $relpath ]]; then
	dir_pane="$(tmux display-message -p -t output '#{pane_current_path}')/"
	abspath=$dir_pane$relpath
	abspath_and_linenum=$abspath' 0'
	open_file
fi

# absolutepath:linenum:
abspath_and_linenum=$(sed -n 's/^\(\/[a-zA-Z0-9_\/\.\-]\+\):\([0-9]\+\):.*$/\1 \2/p' <<< $line)
if [[ -n $abspath_and_linenum ]]; then
	open_file
fi

# relativepath:linenum:
relpath_and_linenum=$(sed -n 's/^\([^\/][a-zA-Z0-9_\/\.\-]\+\):\([0-9]\+\):.*$/\1 \2/p' <<< $line)
if [[ -n $relpath_and_linenum ]]; then
	dir_pane="$(tmux display-message -p -t output '#{pane_current_path}')/"
	abspath_and_linenum=$dir_pane$relpath_and_linenum
	open_file
fi

# python
abspath_and_linenum=$(sed -n 's/^[ ]*File "\([a-zA-Z0-9_\/\.\-]\+\.py\)", line \([0-9]\+\).*$/\1 \2/p' <<< $line)
if [[ -n $abspath_and_linenum ]]; then
	open_file
fi

# git diff
# note: this will take you to the first line in the chunk
linenum=$(sed -n 's/^@@ -[0-9]\+,[0-9]\+ +\([0-9]\+\),[0-9]\+ @@.*$/\1/p' <<< $line)
if [[ -n $linenum ]]; then
	# get the "diff --git a/..." line above the "@@ ..." line
	line_first_part=$(sed -n 's/^\(@@ -[0-9]\+,[0-9]\+ +[0-9]\+,[0-9]\+ @@\).*$/\1/p' <<< $line)
	cmd_tmux_line_diff='tmux copy-mode \; send-keys -X search-backward-text "'$line_first_part'" \; send-keys -X search-backward "^diff --git a/" \; send-keys -X copy-pipe-line-and-cancel \; display-message -p "#{buffer_sample}"'
	line_diff=$(eval $cmd_tmux_line_diff)
	# get filepath
	relpath=$(sed -n 's/^diff --git a\/\([a-zA-Z0-9_\/\.\-]\+\) b\/.*$/\1/p' <<< $line_diff)
	# determine the absolute filepath of a git file, and open the file
	dir_pane="$(tmux display-message -p -t output '#{pane_current_path}')/"
	cmd_dir_git='git -C '$dir_pane' rev-parse --show-toplevel'
	dir_git=$(eval $cmd_dir_git)
	abspath=$dir_git'/'$relpath
	abspath_and_linenum=$abspath' '$linenum
	open_file
fi

# pdflatex & latexmk
linenum=$(sed -n 's/^l\.\([0-9]\+\) .*$/\1/p' <<< $line)
if [[ -n $linenum ]]; then
	cmd_line_relpath='tmux copy-mode \; send-keys -X search-backward "^l.'$linenum' " \; send-keys -X search-backward "^\(\.\/.*\.tex" \; send-keys -X copy-pipe-line-and-cancel \; display-message -p "#{buffer_sample}"'
	line_relpath=$(eval $cmd_line_relpath)
	relpath_tex=$(sed -n 's/^(\.\/\(.*.tex\).*$/\1/p' <<< $line_relpath)
	# check if the file exists in the tmux directory
	dir_pane="$(tmux display-message -p -t output '#{pane_current_path}')/"
	abspath_test=$dir_pane$relpath_tex
	if [[ -e $abspath_test ]]; then
		abspath_and_linenum=$abspath_test' '$linenum
		open_file
	fi
	# check if latexmk changed the directory, and if so, check if the file exists in that directory
	cmd_line_dir_latexmk='tmux copy-mode \; send-keys -X search-backward "^l.'$linenum' " \; send-keys -X search-backward "^Latexmk: Changing directory to " \; send-keys -X copy-pipe-line-and-cancel \; display-message -p "#{buffer_sample}"'
	line_dir_latexmk=$(eval $cmd_line_dir_latexmk)
	dir_latexmk=$(sed -n "s/^Latexmk: Changing directory to '\(.*\)'.*$/\1/p" <<< $line_dir_latexmk)
	abspath_test=$dir_latexmk$relpath_tex
	if [[ -e $abspath_test ]]; then
		abspath_and_linenum=$abspath_test' '$linenum
		open_file
	fi
fi

# at absolutepath:linenum
abspath_and_linenum=$(sed -n 's/^[ ]*at \(\/[a-zA-Z0-9_\/\.\-]\+\):\([0-9]\+\).*$/\1 \2/p' <<< $line)
if [[ -n $abspath_and_linenum ]]; then
	open_file
fi

# no filenames found
echo NO FILENAMES FOUND
