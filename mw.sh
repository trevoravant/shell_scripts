#!/bin/bash
#DIMS="$(xdpyinfo | awk '/dimensions/{print $2}')"
#WIDTH="$(echo $DIMS | sed 's/x.*//')"
#HEIGHT="$(echo $DIMS | sed 's/.*x//')"
#WW=$((WIDTH/2))
get_window_props() {
	OUTPUT=$(xdotool getactivewindow getwindowgeometry)
	MY_ID=$(echo $OUTPUT | awk '{print $2}')
	MY_XY=$(echo $OUTPUT | awk '{print $4}')
	MY_WH=$(echo $OUTPUT | awk '{print $8}')
	MY_X=$(echo $MY_XY | grep -o -P '.*(?=,)')
	MY_Y=$(echo $MY_XY | grep -o -P '(?<=,).*')
	MY_W=$(echo $MY_WH | grep -o -P '.*(?=x)')
	MY_H=$(echo $MY_WH | grep -o -P '(?<=x).*')
	echo MY_X is $MY_X MY_Y is $MY_Y
}

resize_and_move() {
	# inputs: x, y, width, height
	#wmctrl -r :ACTIVE: -e 0,$MY_X,$MY_Y,$TWO_WIDTH,$MY_H
	xdotool windowsize $MY_ID $3 $4
	#xdotool windowsize --usehints $MY_ID 20% 100%
	xdotool windowmove $MY_ID $1 $2
	echo $MY_ID X $1 y $2 w $3 h $4
}

X_MIN=-23
X_MIN=0
Y_MIN=-15
Y_MIN=0
Y_MAX=2190
Y_MAX=2160
X_MAX=3863
X_MAX=3840
W_SCREEN="$(($X_MAX-$X_MIN))"
H_SCREEN="$(($Y_MAX-$Y_MIN))"
ONE_WIDTH="$((W_SCREEN/5))"
#ONE_WIDTH=748
TWO_WIDTH="$(($ONE_WIDTH*2))"
get_window_props
# test
if [ $1 == "test" ]; then
	resize_and_move $ONE_WIDTH $H_SCREEN $MY_X $MY_Y
fi

# move to predtermined grid space
if [ $1 == "grid" ]; then
	MY_X="$(($X_MIN + $2*$ONE_WIDTH))"
	resize_and_move $MY_X $Y_MIN $ONE_WIDTH $H_SCREEN
fi

# expand the window
if [ $1 == "expand" ]; then
	if [ $2 == "right" ]; then
		resize_and_move $MY_X $MY_H $TWO_WIDTH $MY_H
	fi
	if [ $2 == "left" ]; then
		MY_NEW_X="$(($MY_X-$ONE_WIDTH))"
		echo one width
		echo $ONE_WIDTH
		echo my x
		echo $MY_X
		echo my new x
		echo $MY_NEW_X
		echo two width
		echo $TWO_WIDTH
		#wmctrl -r :ACTIVE: -e 0,$MY_NEW_X,$MY_Y,$TWO_WIDTH,$MY_H
		resize_and_move $MY_NEW_X $MY_Y $TWO_WIDTH $MY_H
	fi
fi

# shift the window
if [ $1 == "shift" ]; then
	if [ $2 == "right" ]; then
		MY_NEW_X="$(($MY_X + $ONE_WIDTH))"
		resize_and_move $MY_NEW_X $MY_Y $MY_W $MY_H
	fi
	if [ $2 == "left" ]; then
		MY_NEW_X="$(($MY_X - $ONE_WIDTH))"
		resize_and_move $MY_NEW_X $MY_Y $MY_W $MY_H
	fi
fi
