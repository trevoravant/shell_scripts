#!/bin/bash
# this script takes a python file as its input, finds its correct place in its
# python package, and appropriately calls the python "python -m" command

# get the full path and basename of the python file
full_path=$(realpath $1)
base=$(basename $full_path)

# directories and files for each level
dir=$(dirname $full_path)
folder=$(basename $dir)
init_file="$dir/__init__.py"

# if there is no __init__.py file, then just run python python_file.py 
if [ ! -f $init_file ]; then
	cd $dir
	python $base
	cd -> /dev/null

# else, traverse up the directory until the __init__.py file is not found
else
	mod=$folder.${base::-3} # module, remove .py
	while true; do
		dir=$(dirname $dir) # go up one directory
		folder=$(basename $dir) # only the folder name
		init_file="$dir/__init__.py"
		if [ ! -f $init_file ]; then
		   break
		fi	   
		mod=$folder"."$mod
	done

	# run module as python -m
	cd $dir
	python -m $mod
	cd - > /dev/null # don't print the output
fi

