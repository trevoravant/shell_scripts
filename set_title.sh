#!/bin/bash
# set the title of a gnome-terminal
# https://unix.stackexchange.com/questions/177572/how-to-rename-terminal-tab-title-in-gnome-terminal/186167#186167
if [[ -z "$ORIG" ]]; then
	ORIG=$PS1
fi
TITLE="\[\e]2;$*\a\]"
PS1=${ORIG}${TITLE}
