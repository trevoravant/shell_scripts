#!/bin/bash
thisWID=$(xdotool getactivewindow)
tmux new-session -d -s output # this may fail, but that's ok
numAttached="$(tmux ls -F "#{session_name} #{session_attached}" | grep output | awk '{print $2}')"
if !(( numAttached > 0)); then
	gnome-terminal
	sleep .1
	termWID="$(xdotool search --onlyvisible gnome-terminal | sort -n | tail -n1)"
	xdotool windowactivate $termWID
	xdotool key Super+2
	xdotool type '. set_title.sh output'
	xdotool key Return
	xdotool type 'tmux attach -t output'
	xdotool key Return
	xdotool windowactivate $thisWID
fi
