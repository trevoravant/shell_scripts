#!/bin/bash
# this script takes a python file as its input, finds its correct place in its
# python module, and appropriately calls the python module command (python -m)

# get the full path and basename of the python file
full_path=$(realpath $1)
base=$(basename $full_path)

# directories and files for each level
dir=$(dirname $full_path)
folder=$(basename $dir)
init_file="$dir/__init__.py"
mod=${base::-3} # module, remove .py

# keep going up a directory until the __init__.py file is not found
while [ -f $init_file ]; do
	mod=$folder"."$mod
	dir=$(dirname $dir)
	folder=$(basename $dir)
	init_file="$dir/__init__.py"
done

# run module as python -m
cd $dir
python -m $mod
cd - > /dev/null # don't print the output
