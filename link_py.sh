#!/bin/zsh 
# for python files             
# 
# applied as follows:          
# 
# $ python file.py |& ./link.sh 
# 
# assumes the error output lines look like this: 
# 
#  File "/home/trevor/Downloads/py_test/test.py", line 7, in <module> 
# 
# so the default regex pattern will look something like this: 
# 
#  File "[a-zA-Z0-9_\/\.\-]*\.py", line [0-9]*, in .* 
# 
# note: use the "-u" option in the python command to allow this script to work in real time 
# 
while IFS= read line 
do 
    if echo "$line" | grep -q '  File "[a-zA-Z0-9_\/\.\-]*\.py", line [0-9]*, in .*'; then 
        # get python file path and line number 
        filepath=$(echo $line | sed 's/  File "\([a-zA-Z0-9_\/\.\-]*\.py\)", line [0-9]*, in .*/\1/') 
        num=$(echo $line | sed 's/  File "[a-zA-Z0-9_\/\.\-]*\.py", line \([0-9]*\), in .*/\1/') 
        # create lsh file path 
        lsh=$filepath 
        lsh="${lsh:1}" 
        lsh="${lsh//\//-}" 
        lsh="${lsh//./-}" 
        lsh=$lsh-$num 
        lsh=/tmp/$lsh.lsh 
        touch $lsh 
        chmod +x $lsh 
        # create link 
        pre='  File "' 
        post=$(echo $line | sed 's/  File "[a-zA-Z0-9_\/\.\-]*\.py\(", line [0-9]*, in .*\)/\1/') 
        link='\e]8;;file://'$lsh'\e\\'$filepath'\e]8;;\e\\' 
        line_link=$pre$link$post 
        echo $line_link 
    else 
        echo $line 
    fi 
done 
# fill lsh file 
echo \ 
"if vim --serverlist | grep -q VIM; then 
  vim --remote +"$num $filepath" 
else 
  kgx -- sh -c 'vim +"$num $filepath"' 
fi"\ 
> $lsh
